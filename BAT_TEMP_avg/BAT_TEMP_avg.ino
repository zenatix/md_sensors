//Firmware version : BLT1.0     - only battery and temp LM35

#include <SimpleModbusSlave_DUE.h>
#include <EEPROM.h>
#include <Timer.h>
#include <OneWire.h>
#include <DallasTemperature.h>

//b17 think valley building first floor sector 32 gurgaon
//122001

//fuse bits   low:FF   ,high: DA   ,Ext: 05
// DS18B20 pin
#define ONE_WIRE_BUS 10 
#define REED  A0
#define BUZZ  9
#define MODBUS_ID_LOC 1
#define MODBUS_CONFIG_LOC 4
#define MODBUS_OK  1
#define MODBUS_DEFAULT_ID 1


#define HIGH_   (1)
#define LOW_    (0)
 
#define   RELAY1      (3)
#define   RELAY2      (4)
#define   RELAY3      (8)
#define   RELAY4      (7)
#define   CT1         (A1)
#define   CT2         (A0)
#define   CT3         (A6)
#define   CT4         (A7)
#define   TX_EN       (12)
#define   TEMP_RST    (8)
#define   TEMP        (10)

//////////////// registers of your slave ///////////////////
enum 
{     
  // just add or remove registers and your good to go...
  // The first register starts at address 0
  MB_0_R,     
  MB_1_R,
  MB_2_R,
  MB_3_R,        
  MB_4_R,     
  MB_5_R,
  MB_6_R,
  MB_7_R,        
  MB_8_R,         
  MB_9_T,
  MB_10_RE,
  MB_11_L,
  BATTERY,        //address 12
  MB_13_buzz,     // Register for enabling the buzzer functionnality 0 default means enable, 1 disables it
  MB_14_NOTUSED,
  MB_15_NOTUSED,
  MB_16_REBOOTCNT,
  MB_17_MODBUSID,
  MB_18_THRESHOLD,
  MB_19_HEARTBEAT,
  MB_20_CSD,/*CSD-> Current Sensor Digital Value*/
  MB_21_CSD,
  MB_22_CSD,
  MB_23_CSD,
  MB_24_CSD,
  MB_25_CSD,
  MB_26_CSD,
  MB_27_CSD,
  MB_28_CSD,
  MB_29_NOTUSED,
  MB_30_CSA,
  MB_31_CSA,/*CSA-> Current Sensor Analog Value*/
  MB_32_CSA,
  MB_33_CSA,
  MB_34_CSA,
  MB_35_CSA,
  MB_36_CSA,
  MB_37_CSA,
  MB_38_CSA,
  MB_39_TEMPRST,
  MB_40_TEMP_0_ADD,
  MB_41_TEMP_0_ADD,
  MB_42_TEMP_0_ADD,
  MB_43_TEMP_0_ADD,
  MB_44_TEMP_0_VAL,
  MB_40_TEMP_1_ADD,
  MB_41_TEMP_1_ADD,
  MB_42_TEMP_1_ADD,
  MB_43_TEMP_1_ADD,
  MB_44_TEMP_1_VAL,
  MB_40_TEMP_2_ADD,
  MB_41_TEMP_2_ADD,
  MB_42_TEMP_2_ADD,
  MB_43_TEMP_2_ADD,
  MB_44_TEMP_2_VAL,
  HOLDING_REGS_SIZE // leave this one
};

// Setup a oneWire instance to communicate with any OneWire devices 
// (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
 
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

unsigned int holdingRegs[HOLDING_REGS_SIZE];
int reboot_val;
static unsigned int heartbeatCount=0;
static unsigned char modbusId=0;
float R1=100.0 , R2 = 2.0;   // Values in kilo-ohm
int bat_mean[30]; //hold the value of bat every sec 
int cnt =0;
/*Timer instance */
Timer tm;
 /*
 This function will calculate the actual battery voltage and return multiplying by 100. 
 R1 and R2 are the divider resisters.
 */
 #define        TEMP_ATTEMPT      5
 
void temp_reset(){
 digitalWrite(TEMP_RST,LOW);         // write low at the base of transistor
  for(uint8_t i=1;i!=0;i++);          // wait for a while
  digitalWrite(TEMP_RST,HIGH);        // make it high again
}

float get_temp(void){
   uint8_t temp_err = 0;     // local error counter
    float tempC = 00.00;      // to store the value of current temperature
here: sensors.requestTemperatures();      // request the temperature sensor for reading
    tempC = sensors.getTempCByIndex(0);     // get the value of temperature connected to the first index
    if((tempC>-55 && tempC<125)&&(tempC!=85)){    // If temperature gets read with within true range
      return tempC;               // and return
    }
    else {
      temp_reset();         // Else reset the temperature sensor
      if(temp_err<TEMP_ATTEMPT){    // check if all reading attempt has been made
        temp_err ++;        // increase the value of temperature error
        goto here;          // read again
      }
      else {
        if(tempC == -127)return 200;  // return this error value
        else return tempC;        // return 85, if this error has been passed
      }
  }
}

int GetBattVoltage(void)
{
  float Vo;
  int count;
  float bat;
  count = analogRead(A1);
  Vo = (5.0 * count)/1023;
  bat = (R1+R2)/R2;
  return Vo*bat*100;  //return actual battery voltage  *100
}

int sort_bat_mean(void)
{
  int temp=0;//,i,j;
  int i = 0;
 for(i=cnt;i>=0;i--){
  temp += bat_mean[i]; 
 }
 temp = (temp/cnt);
 cnt = 0;
 return temp;
}

void ConfigTemp(void){
 pinMode(TEMP_RST,OUTPUT);
}


void HeartBeatEvent(void){
  bat_mean[cnt] = GetBattVoltage(); 
  cnt++;
  heartbeatCount++;
  heartbeatCount&=0xFFFF;
  holdingRegs[MB_19_HEARTBEAT]=heartbeatCount; 
}

void UpdateRebootCount(void){
  /*Get old reboot value*/
  reboot_val = (int)EEPROM.read(10);
  /*If first time or memory full condition occures then*/
  if (reboot_val==0xFF){
    /*reset the counter value*/
    reboot_val = 0;
  }
  else{
    /* inc by one*/
    reboot_val+=1;
  }
  /*Save the value in EEPROM*/
  EEPROM.write(10,reboot_val);
  //EEPROM.commit();
  /* Update the corresponding Holding register*/
  holdingRegs[MB_16_REBOOTCNT] = reboot_val;

}

void SetModbusId(unsigned char id){
  EEPROM.write(MODBUS_ID_LOC,id);
  holdingRegs[MB_17_MODBUSID] = modbusId;
}

void GetModbusId(void){
  unsigned char temp;
  temp =EEPROM.read(MODBUS_CONFIG_LOC);
  if(temp==MODBUS_OK){
     modbusId=EEPROM.read(MODBUS_ID_LOC);
     holdingRegs[MB_17_MODBUSID] = modbusId;
  }
  else{
    EEPROM.write(MODBUS_ID_LOC,MODBUS_DEFAULT_ID);
    EEPROM.write(MODBUS_CONFIG_LOC,MODBUS_OK);
    holdingRegs[MB_17_MODBUSID] = modbusId;
  }
}



void setup(){
  /*set all registers to ones*/
  memset(holdingRegs,0,HOLDING_REGS_SIZE);
  UpdateRebootCount();
  /*Configure the timer for heartbeat counter*/
  tm.every(1000,HeartBeatEvent);
  /*Get the stored modbus id*/
  GetModbusId();
  Serial.print(modbusId,DEC);
  /*parameters(SerialPort,baudrate, ID, tx_en,Holding_reg_size,Holding_reg_addr)*/
  modbus_configure(&Serial, 9600,MODBUS_DEFAULT_ID,TX_EN,HOLDING_REGS_SIZE, holdingRegs);
  modbus_update_comms(9600,modbusId);
   // Start up the library
  pinMode(TEMP_RST,OUTPUT);       // set pins for reset
  pinMode(BUZZ,OUTPUT);
  TCCR1B = (TCCR1B & 0b11111001); // set for PWM 
  pinMode(REED,INPUT);       // set pins for reset
  digitalWrite(TEMP_RST,HIGH);      // write the high value
  sensors.begin();           // initialize the sensor
  holdingRegs[BATTERY] = GetBattVoltage();
}


void buzzer(void){
  uint8_t i;
  for(i=0;i!=10;i++){ 
   analogWrite(BUZZ,i);
   delay(2); 
  }
  for(i=10;i!=0;i--){
    analogWrite(BUZZ,i);
    delay(2);
  }
  analogWrite(BUZZ,0);
  digitalWrite(BUZZ,0);
}

void loop(){
  /*process relay if modbus holding register is modified*/
 if(cnt>=10){
  holdingRegs[BATTERY] = sort_bat_mean();    // return mean value from array
  cnt = 0;
 }
  holdingRegs[MB_9_T] = get_temp()*100;
  holdingRegs[MB_10_RE] = digitalRead(REED); 
  /*update the timer so that HeartBeatEvent can fire*/
    if (digitalRead(REED)&&(!holdingRegs[MB_13_buzz])){
      buzzer();
    }
  tm.update();
}

void serialEvent() {
   modbus_update_new();
}


